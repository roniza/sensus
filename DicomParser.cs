﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rzdcxLib;
using System.IO;
using System.Runtime.InteropServices;
using GenericDICOM;

/*
 A simple file creator.
 File format: 

    double voxelWidth (x mm)
    double voxelHeight (y mm)
    double voxelDepth (z mm)
    Uint32 volumeRows (y)
    Uint32 volumeColumns (x)
    Uint32 volumeSlices (z)
    Uint16[] volumeData (pixels)

    Uses MODALIZER SDK and GenericDICOM
 */
namespace DicomParser
{
    // One file enntry with the DICOM object, elemennts used as a key and elemennts that used for sort 
    // currently assumes one study with one series in one directionn in every scan and the directionn is Z (AXIAL scan)
    class FileEntry : DicomModule
    {
        // The DICOM Object
        DCXOBJ _obj;

        // Filename (just for testing)
        string _filename;

        // Image Position patient 
        double x, y, z;

        public double Z
        {
            get { return z; }
        }

        // Ctor from DICOM object extracts the data to memebers
        // Also parse the image positionn into x,y,z 
        public FileEntry(DCXOBJ obj, string filename)
        {
            _obj = obj;
            _filename = filename;

            this.FromDicomObject(obj);
            string[] split = ImagePositionPatient.Split('\\');
            x = Double.Parse(split[0]);
            y = Double.Parse(split[1]);
            z = Double.Parse(split[2]);
        }

        // All these attributes are assumed to be identical for all files.
        // Later you cann break it so every scan can create multiple  files

        [Tag(DICOM_TAGS_ENUM.studyInstanceUID)]
        public string StudyInstanceUID;

        [Tag(DICOM_TAGS_ENUM.seriesInstanceUID)]
        public string SeriesInstanceUID;

        [Tag(DICOM_TAGS_ENUM.FrameOfReferenceUID)]
        public string FrameOfReferenceUID;

        // From Image Pixel Module. We require that all images are the same size
        [Tag(DICOM_TAGS_ENUM.Rows)]
        public ushort Rows;

        [Tag(DICOM_TAGS_ENUM.Columns)]
        public ushort Columns;

        // These are the Image Plane Module membbers
        // We require that only the position channges in Z
        // See http://dicom.nema.org/medical/dicom/current/output/chtml/part03/sect_C.7.6.2.html
        [Tag(DICOM_TAGS_ENUM.PixelSpacing)]
        public string PixelSpacing;

        [Tag(DICOM_TAGS_ENUM.ImageOrientationPatient)]
        public string ImageOrientationPatient;


        // Image position patient can vary between the files and we sort usinng the Z compoennt
        [Tag(DICOM_TAGS_ENUM.ImagePositionPatient)]
        public string ImagePositionPatient;

        // We use ToString for sorting
        // All these should be the same for all images
        public override string ToString()
        {
            return
                //patientModule.ToString() +
                StudyInstanceUID.ToString() +
                SeriesInstanceUID.ToString() +
                FrameOfReferenceUID.ToString() +
                PixelSpacing.ToString() +
                Rows.ToString() +
                Columns.ToString() +
                ImageOrientationPatient.ToString();// +
        }

        // Note the precision is 100000
        public int CompareTo(FileEntry other)
        {
            int keyRes = this.ToString().CompareTo(other.ToString());
            if (keyRes == 0)
                return (int)((this.z - other.z) * 100000); // we assume here that x and y will be identical for now
            else
                throw new ApplicationException("Files are not a single Volume. Check the keys.");
        }

        // Get the pixels array. This is why we kept the DCXOBJ for
        public ushort[] GetPixels()
        {
            return _obj.GetElement((int)DICOM_TAGS_ENUM.PixelData).ValueArray;
        }
    }

    // A volume
    class Volume
    {
        public double voxelWidth;// (x mm)
        public double voxelHeight;// (y mm)
        public double voxelDepth;// (z mm)
        public UInt32 volumeRows;// (y)
        public UInt32 volumeColumns;// (x)
        public UInt32 volumeSlices;// (z)
        //UInt16[] volumeData; // lets not keep it in memory
    }

    // A simple step bby step processor to Scan a directory
    class DicomScanner
    {
        // The directory to scan
        string path;

        // The DICOM objects. One entry per file
        List<FileEntry> collection = new List<FileEntry>();
        
        // The NES file volume attriutes
        Volume volume = new Volume();
       
        // Ctor just gets the path to scann
        public DicomScanner(string in_path)
        {
            path = in_path;
        }

        // Do the actual scan
        // Only collect files with .dcm suffix
        // Only take AXIAL CT images
        public void Scan()
        {
            var files = Directory.EnumerateFiles(path, "*.dcm");
            foreach (string file in files)
            {
                try
                {
                    DCXOBJ obj = new DCXOBJ();
                    obj.openFile(file);
                    if (obj.GetElement((int)DICOM_TAGS_ENUM.sopClassUid).Value == "1.2.840.10008.5.1.4.1.1.2" &&
                        obj.GetElement((int)DICOM_TAGS_ENUM.ImageType).Value.StartsWith("ORIGINAL\\PRIMARY\\AXIAL"))
                    {
                        FileEntry entry = new FileEntry(obj, file);
                        collection.Add(entry);
                    }
                }
                catch (COMException e)
                {
                    Console.WriteLine("Error when processing file: {0}, Error description: {1}", file, e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error when processing file: {0}, Error description: {1}", file, e.Message);
                }
            }
        }

        int sort_direction;
        // Sort the files
        public void Sort(int direction=-1)
        {
            sort_direction = direction;
            collection.Sort(FileEntryComparer);
        }

        private int FileEntryComparer(FileEntry x, FileEntry y)
        {
            int keyRes = x.ToString().CompareTo(y.ToString());
            if (keyRes == 0)
                return (int)((x.Z - y.Z) * 100000 * sort_direction); // we assume here that x and y will be identical for now
            else
                throw new ApplicationException("Files are not a single Volume. Check the keys.");
        }

        /// <summary>
        ///  Fill the volume file header
        /// </summary>
        public void Calculate()
        {
            if (collection.Count >= 2)
            {
                string a=collection[0].ImagePositionPatient.Split('\\')[2];
                double d1 = Double.Parse(collection[0].ImagePositionPatient.Split('\\')[2]);
                double d2 = Double.Parse(collection[1].ImagePositionPatient.Split('\\')[2]);
                volume.voxelDepth = Math.Abs(d2 - d1);
                volume.voxelWidth = Double.Parse(collection[0].PixelSpacing.Split('\\')[0]);
                volume.voxelHeight = Double.Parse(collection[0].PixelSpacing.Split('\\')[1]);
                volume.volumeRows = collection[0].Rows;
                volume.volumeColumns = collection[0].Columns;
                volume.volumeSlices = (uint)collection.Count;
            }
        }

        // Save the volume file 
        public void SaveVolume(string pathToSave)
        {
            FileStream fs = new FileStream(pathToSave, FileMode.Create);
            BinaryWriter writer = new BinaryWriter(fs);
            writer.Write(volume.voxelWidth);
            writer.Write(volume.voxelWidth);
            writer.Write(volume.voxelDepth);
            writer.Write(volume.volumeRows);
            writer.Write(volume.volumeColumns);
            writer.Write(volume.volumeSlices);
            foreach (FileEntry f in collection)
            {
                ushort[] pixels = f.GetPixels();
                // There must be a better way to write ushort array to file
                foreach (ushort p in pixels)
                    writer.Write(p);
            }
            writer.Close();
            writer.Dispose();
            fs.Close();
            fs.Dispose();
        }

    }
    class Program
    { 

        /// <summary>
        /// Main function: 
        /// - Handle command line arguments
        /// - Scan the directory
        /// - Sort the entries
        /// - Calculate the values
        /// - Save the file
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                if (args.Length < 2)
                {
                    Console.WriteLine("Usage: Scannner in-directory out-file-name [direction]");
                    return;
                }
                string pathToScan = args[0];
                string pathToSave = args[1];

                int direction = -1;
                if (args.Length > 2)
                {
                    if (!Int32.TryParse(args[2], out direction))
                    {
                        Console.WriteLine("directionn must be 1 or -1");
                        return;
                    }
                }

                DicomScanner scanner = new DicomScanner(pathToScan);
                scanner.Scan();
                scanner.Sort(direction);
                scanner.Calculate();
                scanner.SaveVolume(pathToSave);
            }
            catch (ApplicationException ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
